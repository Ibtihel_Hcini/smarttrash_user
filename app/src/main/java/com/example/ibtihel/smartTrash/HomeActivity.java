package com.example.ibtihel.smartTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private CardView time_card, reclam_card, notif_card, infos_card;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // defining cards
        time_card = (CardView) findViewById(R.id.time_card);
        reclam_card = (CardView) findViewById(R.id.reclamation_card);
        notif_card = (CardView) findViewById(R.id.notification_card);
        infos_card = (CardView) findViewById(R.id.info_card);

        // add action listner

        time_card.setOnClickListener(this);
        reclam_card.setOnClickListener(this);
        notif_card.setOnClickListener(this);
        infos_card.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        Intent i ;
        switch ( v.getId()) {
            case  R.id.time_card :
                i= new Intent(this, MapActivity.class);
                startActivity(i);
                break;

            case  R.id.reclamation_card : ;
                i= new Intent(this, ReclamActivity.class);
                startActivity(i);
                break;

            case  R.id.notification_card :

            i= new Intent(this, NotifActivity.class);
            startActivity(i);
                break;

            case  R.id.info_card :
                i= new Intent(this, InfoActivity.class);
                startActivity(i);
                break;


        }


    }
}
