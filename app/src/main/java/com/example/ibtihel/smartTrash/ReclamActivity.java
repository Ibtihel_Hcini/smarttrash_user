package com.example.ibtihel.smartTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class ReclamActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reclam);

        Spinner Zones = (Spinner) findViewById(R.id.zone);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(ReclamActivity.this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.zones));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Zones.setAdapter(myAdapter);
    }


    public void reclam(View view) {



        Spinner Zones = (Spinner) findViewById(R.id.zone);
        final EditText nom=(EditText)findViewById(R.id.nom_id);
        final EditText prenom=(EditText)findViewById(R.id.prenom_id);
        final EditText tel=(EditText)findViewById(R.id.tel_id);
        final EditText msg=(EditText)findViewById(R.id.message_id);



        String selectedZone=Zones.getSelectedItem().toString();
        String nomStr = nom.getText().toString();
        String prenomStr = prenom.getText().toString();
        String telStr = tel.getText().toString();
        String messageStr = msg.getText().toString();


        DatabaseReference mDatabase;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Reclamation reclamation = new Reclamation(selectedZone, nomStr, prenomStr, telStr, messageStr);

        mDatabase.child("reclamations").push().setValue(reclamation);




        Intent intent=new Intent(getApplicationContext(),Reclam2Activity.class);
        intent.putExtra("nomClient", nomStr);
        startActivity(intent);



    }





}


