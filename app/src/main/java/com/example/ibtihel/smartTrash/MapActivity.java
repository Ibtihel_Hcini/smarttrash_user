package com.example.ibtihel.smartTrash;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.CompoundButton;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
     * An activity that displays a Google map with a marker (pin) to indicate a particular location.
     */
    public class MapActivity extends AppCompatActivity
            implements OnMapReadyCallback {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Retrieve the content view that renders the map.
            setContentView(R.layout.activity_map);
            // Get the SupportMapFragment and request notification
            // when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }

        /**
         * Manipulates the map when it's available.
         * The API invokes this callback when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera. In this case,
         * we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user receives a prompt to install
         * Play services inside the SupportMapFragment. The API invokes this method after the user has
         * installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(final GoogleMap googleMap) {
            // Add a marker in Sydney, Australia,
            // and move the map's camera to the same location.
            LatLng OnsCity = new LatLng(34.8327762, 10.7555032);



            //zone 1 :
            LatLng t1_1 = new LatLng(34.842094, 10.760394);
            LatLng t1_2 = new LatLng(34.841240, 10.758709);
            LatLng t1_3 = new LatLng(34.839039, 10.758655);
            LatLng t1_4 = new LatLng(34.839198, 10.760878);
            LatLng t1_5 = new LatLng(34.836926, 10.760357);
            LatLng t1_6 = new LatLng(34.835762, 10.761349);

            //zone 2 :
            LatLng t2_1 = new LatLng(34.839480, 10.755873);
            LatLng t2_2 = new LatLng(34.838441, 10.754047);
            LatLng t2_3 = new LatLng(34.838203, 10.756945);
            LatLng t2_4 = new LatLng(34.835729, 10.754463);
            LatLng t2_5 = new LatLng(34.835021, 10.757286);
            LatLng t2_6 = new LatLng(34.834783, 10.759454);
            LatLng t2_7 = new LatLng(34.833999, 10.758048);

            //zone 3 :
            LatLng t3_1 = new LatLng(34.832042, 10.761600);
            LatLng t3_2 = new LatLng(34.833946, 10.762653);
            LatLng t3_3 = new LatLng(34.832553, 10.759208);
            LatLng t3_4 = new LatLng(34.830900, 10.757305);
            LatLng t3_5 = new LatLng(34.830809, 10.763058);
            LatLng t3_6 = new LatLng(34.830608, 10.761636);
            LatLng t3_7 = new LatLng(34.830234, 10.760040);
            LatLng t3_8 = new LatLng(34.829328, 10.758944);
            LatLng t3_9 = new LatLng(34.828767, 10.759911);
            LatLng t3_10 = new LatLng(34.828200, 10.758405);
            LatLng t3_11 = new LatLng(34.827704, 10.760111);
            LatLng t3_12 = new LatLng(34.826402, 10.760970);
            LatLng t3_13 = new LatLng(34.825626, 10.762666);
            LatLng t3_14 = new LatLng(34.825143, 10.760538);
            LatLng t3_15 = new LatLng(34.824921, 10.761812);

            //zone 4 :
            LatLng t4_1 = new LatLng(34.829761, 10.755293);
            LatLng t4_2 = new LatLng(34.829337, 10.755648);
            LatLng t4_3 = new LatLng(34.828613, 10.756570);
            LatLng t4_4 = new LatLng(34.827962, 10.755142);
            LatLng t4_5 = new LatLng(34.827071, 10.752700);
            LatLng t4_6 = new LatLng(34.826474, 10.753160);
            LatLng t4_7 = new LatLng(34.826474, 10.754436);
            LatLng t4_8 = new LatLng(34.825711, 10.755163);
            LatLng t4_9 = new LatLng(34.825060, 10.753966);
            LatLng t4_10 = new LatLng(34.824540, 10.755266);
            LatLng t4_11 = new LatLng(34.825848, 10.757382);
            LatLng t4_12 = new LatLng(34.825210, 10.758945);
            LatLng t4_13 = new LatLng(34.823909, 10.758396);
            LatLng t4_14 = new LatLng(34.823079, 10.756385);

            //zone 5:
            LatLng t5_1 = new LatLng(34.835921, 10.747341);
            LatLng t5_2 = new LatLng(34.835107, 10.746587);
            LatLng t5_3 = new LatLng(34.835629, 10.748441);
            LatLng t5_4 = new LatLng(34.834201, 10.751619);
            LatLng t5_5 = new LatLng(34.832733, 10.752266);
            LatLng t5_6 = new LatLng(34.830964, 10.749756);

            //zone 6:
            LatLng t6_1 = new LatLng(34.826775, 10.750872);
            LatLng t6_2 = new LatLng(34.826617, 10.748823);
            LatLng t6_3 = new LatLng(34.825912, 10.749436);
            LatLng t6_4 = new LatLng(34.825630, 10.747161);
            LatLng t6_5 = new LatLng(34.824978, 10.747773);
            LatLng t6_6 = new LatLng(34.824960, 10.746024);

            //zone 7:
            LatLng t7_1 = new LatLng(34.825434, 10.743721);
            LatLng t7_2 = new LatLng(34.826137, 10.740004);
            LatLng t7_3= new LatLng(34.824775, 10.738618);
            LatLng t7_4 = new LatLng(34.824770, 10.740816);
            LatLng t7_5 = new LatLng(34.824110, 10.739645);
            LatLng t7_6 = new LatLng(34.821793, 10.740443);



            final LatLng[] markers = {t1_1, t1_2, t1_3, t1_4, t1_5, t1_6,
                    t2_1, t2_2, t2_3, t2_4, t2_5, t2_6,t2_7,
                    t3_1, t3_2, t3_3, t3_4, t3_5, t3_6,t3_7,t3_8, t3_9, t3_10, t3_11, t3_12, t3_13,t3_14,t3_15,
                    t4_1, t4_2, t4_3, t4_4, t4_5, t4_6,t4_7,t4_8, t4_9, t4_10, t4_11, t4_12, t4_13,t4_14,
                    t5_1, t5_2, t5_3, t5_4, t5_5, t5_6,
                    t6_1, t6_2, t6_3, t6_4, t6_5, t6_6,
                    t7_1, t7_2, t7_3, t7_4, t7_5, t7_6 };

            final String[] markers_String = {
                    "t1_1","t1_2","t1_3","t1_4","t1_5","t1_6",
                    "t2_1","t2_2","t2_3","t2_4","t2_5","t2_6","t2_7",
                    "t3_1","t3_2","t3_3","t3_4","t3_5","t3_6","t3_7","t3_8","t3_9","t3_10","t3_11","t3_12","t3_13","t3_14","t3_15",
                    "t4_1","t4_2","t4_3","t4_4","t4_5","t4_6","t4_7","t4_8","t4_9","t4_10","t4_11","t4_12","t4_13","t4_14",
                    "t5_1","t5_2","t5_3","t5_4","t5_5","t5_6",
                    "t6_1","t6_2","t6_3","t6_4","t6_5","t6_6",
                    "t7_1","t7_2","t7_3","t7_4","t7_5","t7_6"};




            // DatabaseReference
            final DatabaseReference mDatabase;
            mDatabase = FirebaseDatabase.getInstance().getReference();
            for (int i=0;i<markers.length;i++)
            {
                final int j=i;
                mDatabase.child("VirtualTrashs").child(markers_String[i].toString()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String value = dataSnapshot.getValue().toString();
                        if (value.equals("1"))
                            googleMap.addMarker(new MarkerOptions().position(markers[j]).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
                        else
                            googleMap.addMarker(new MarkerOptions().position(markers[j]).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
                    }

                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        throw firebaseError.toException();

                    }
                });


            }






            /*

            //Marker for Zone 1
            //googleMap.addMarker(new MarkerOptions().position(t1_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t1_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t1_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t1_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t1_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t1_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 2
            googleMap.addMarker(new MarkerOptions().position(t2_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t2_7).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 3
            googleMap.addMarker(new MarkerOptions().position(t3_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_7).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_8).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_9).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_10).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_11).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_12).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_13).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_14).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t3_15).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 4
            googleMap.addMarker(new MarkerOptions().position(t4_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_7).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_8).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_9).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_10).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_11).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_12).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_13).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t4_14).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 5
            googleMap.addMarker(new MarkerOptions().position(t5_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t5_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t5_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t5_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t5_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t5_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 6
            googleMap.addMarker(new MarkerOptions().position(t6_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t6_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t6_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t6_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t6_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t6_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            //Marker for Zone 7
            googleMap.addMarker(new MarkerOptions().position(t7_1).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t7_2).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t7_3).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));
            googleMap.addMarker(new MarkerOptions().position(t7_4).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t7_5).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.fulltrash)));
            googleMap.addMarker(new MarkerOptions().position(t7_6).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.emptytrash)));

            */








            CameraUpdate cameraPosition = CameraUpdateFactory.newLatLngZoom(OnsCity, 14);
            googleMap.moveCamera(cameraPosition);
            googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);


        }

        private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId)
        {
            Drawable vectorDrawable= ContextCompat.getDrawable(context,vectorResId);
            vectorDrawable.setBounds(0,0,vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
            Bitmap bitmap=Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas=new Canvas(bitmap);
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }
    }
