package com.example.ibtihel.smartTrash;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class Map2Activity extends AppCompatActivity implements View.OnClickListener{

    private CardView bnCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map2);



        bnCard = (CardView) findViewById(R.id.homeBtn);
        bnCard.setOnClickListener(this);
    }



    public void onClick(View v) {
        Intent i= new Intent(this, HomeActivity.class);
        startActivity(i);

    }
}
