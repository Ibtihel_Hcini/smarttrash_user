package com.example.ibtihel.smartTrash;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Reclamation {

    public String zone;
    public String nom;
    public String prenom;
    public String tel;
    public String message;

    public Reclamation() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Reclamation(String zone, String nom, String prenom, String tel, String message) {
        this.zone = zone;
        this.nom = nom;
        this.prenom = prenom;
        this.tel = tel;
        this.message = message;
    }
}
